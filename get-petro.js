const fetch = require("node-fetch");

let url = 'https://petroapp-price.petro.gob.ve/price/';
let init = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
	coins: ["PTR"],
	fiats: ["Bs","USD"]})
};

(async()=>{
  const rawResponse = await fetch(url,init);
  const response = await rawResponse.json();
  console.log(response.data);
})();