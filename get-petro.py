import json
import requests

values = {}
url = 'https://petroapp-price.petro.gob.ve/price/'
headers = {'Accept':'application/json','Content-Type':'application/json'}
values['coins'] = ["PTR"]
values['fiats'] = ["Bs","USD"]
response = {}

try:
	# sending get request and saving the response as response object
	r = requests.post(url=url, data=json.dumps(values), headers=headers)
	# extracting data in json format
	data = r.json()
	# show data
	response = data['data']
except requests.exceptions.RequestException as e:
	print(e)
  
print(response['PTR']['BS'])
